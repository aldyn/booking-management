package fe.tuzla.master.nbp.common;

import fe.tuzla.master.nbp.object.DateIntervalHolder;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DateUtils {
  private static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
  private static SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm:ss");

  public static Date parse(String strDate) {
    Date date = null;
    try {
      date = sdf.parse(strDate);
    } catch (ParseException ignored) {
    } catch (NullPointerException ignored) {
    }
    return date;
  }

  public static Date parseTime(String strTime) {
    Date date = null;
    try {
      date = sdfTime.parse(strTime);
    } catch (ParseException ignored) {
    } catch (NullPointerException ignored) {
    }
    return date;
  }

  public static String format(Date date) {
    return date == null ? "" : sdf.format(date);
  }

  public static String formatTime(Date time) {
    return time == null ? "" : sdfTime.format(time);
  }

  public static String getTimeDiffAsStr(Date startTime, Date endTime) {
    if (endTime != null) {
      long diff = Math.abs(endTime.getTime() - startTime.getTime());
      int diffSeconds = (int) diff / 1000 % 60;
      int diffMinutes = (int) diff / (60 * 1000) % 60;
      int diffHours = (int) diff / (60 * 60 * 1000);
      return new Time(diffHours, diffMinutes, diffSeconds).toString();
    }
    return "";
  }

  private static Date getTimeDiff(Date startTime, Date endTime) {
    if (endTime != null) {
      long time = Math.abs(endTime.getTime() - startTime.getTime());

      return new Date(time - 60 * 60 * 1000L);
    }
    return null;
  }

  public static <T extends DateIntervalHolder> String sumIntervals(List<T> items) {
    Date sum = null;
    if (items == null) {
      return null;
    }
    for (T item : items) {
      Date time = getTimeDiff(item.getStartTime(), item.getEndTime());
      if (time != null) {
        if (sum == null) {
          sum = time;
        } else {
          long tempTime = sum.getTime() + time.getTime();
          sum = new Date(tempTime + 60 * 60 * 1000L);
        }
      } else {
        long elapsedTime = new Date().getTime() - item.getStartTime().getTime() - 60 * 60 * 1000L;
        long tempTime = sum != null ? sum.getTime() + elapsedTime : elapsedTime - 60 * 60 * 1000L;
        sum = new Date(tempTime + 60 * 60 * 1000L);
      }
    }
    if (sum == null) {
      return "";
    }
    int diffSeconds = sum.getSeconds();
    int diffMinutes = sum.getMinutes();
    int diffHours = sum.getHours();
    return new Time(diffHours, diffMinutes, diffSeconds).toString();
  }
}
