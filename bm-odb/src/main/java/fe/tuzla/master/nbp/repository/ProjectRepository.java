package fe.tuzla.master.nbp.repository;

import com.db4o.query.Predicate;
import fe.tuzla.master.nbp.common.CommonRepository;
import fe.tuzla.master.nbp.object.ProjectObject;

import javax.ejb.Stateless;

@Stateless
public class ProjectRepository extends CommonRepository {

    public ProjectRepository() {
        super(ProjectObject.class);
    }

    public ProjectObject findById(Integer id) {
        return findAny(matchById(id));
    }

    private Predicate<ProjectObject> matchById(final Integer id) {
        return new Predicate<ProjectObject>() {
            public boolean match(ProjectObject project) {
                return project.getId().equals(id);
            }
        };
    }
}
