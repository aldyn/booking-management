package fe.tuzla.master.nbp.common;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import fe.tuzla.master.nbp.api.Db4oApi;
import java.util.List;

public abstract class CommonRepository {
  protected ObjectContainer getContainer() {
    return Db4oApi.getInstance().getContainer();
  }

  private Class clazz_;

  public CommonRepository(Class clazz) {
    clazz_ = clazz;
  }

  public void rollback() {
    ObjectContainer container = getContainer();
    container.rollback();
  }

  public <T> T save(T data) {
    ObjectContainer container = getContainer();
    container.store(data);
    container.commit();
    return data;
  }

  public <T> T read(final T data) {
    ObjectContainer container = getContainer();
    List<T> result =
        container.query(
            new Predicate<T>() {
              public boolean match(T o) {
                return o.equals(data);
              }
            });
    return result.size() == 1 ? result.get(0) : null;
  }

  public <T> void remove(T data) {
    ObjectContainer container = getContainer();
    container.delete(data);
    container.commit();
  }

  public <T> void removeAll(Class<T> objectClass) {
    ObjectContainer container = getContainer();
    ObjectSet<T> results = container.query(objectClass);
    for (T result : results) {
      container.delete(result);
    }
    container.commit();
  }

  public <T> List<T> find(Predicate<T> predicate) {
    ObjectContainer container = getContainer();
    return container.query(predicate);
  }

  public <T> T findAny(Predicate<T> predicate) {
    List<T> obj = find(predicate);
    return obj.isEmpty() ? null : obj.get(0);
  }

  public <T> List<T> findAll() {
    ObjectContainer container = this.getContainer();
    return container.query(clazz_);
  }

  public Integer getNextId() {
    return findAll().size();
  }
}
