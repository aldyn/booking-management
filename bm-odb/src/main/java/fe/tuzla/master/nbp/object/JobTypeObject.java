package fe.tuzla.master.nbp.object;

public class JobTypeObject implements IdHolder{
    private Integer id_;
    private String name_;
    private String description_;

    public JobTypeObject(Integer id, String name, String description) {
        id_ = id;
        name_ = name;
        description_ = description;
    }

    public Integer getId() {
        return id_;
    }

    public void setId(Integer id) {
        id_ = id;
    }

    public String getName() {
        return name_;
    }

    public void setName(String name) {
        name_ = name;
    }

    public String getDescription() {
        return description_;
    }

    public void setDescription(String description) {
        description_ = description;
    }
}
