package fe.tuzla.master.nbp.api;

import com.db4o.Db4o;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

public class Db4oApi {

  private ObjectContainer container;

  private static Db4oApi instance;

  private Db4oApi() {}

  public static Db4oApi getInstance() {
    if (instance == null) {
      instance = new Db4oApi();
    }
    return instance;
  }

  public ObjectContainer getContainer() {
    return this.container;
  }

  public void openConnection() {
    if (this.container == null) {
      this.container =
          Db4oEmbedded.openFile("../../../bookingManagement.db4o");
      // globalno postavljanje dubine aktivacije na 1
      this.container.ext().configure().activationDepth(2);
    }
  }

  public void closeConnection() {
    this.getContainer().close();
    this.container = null;
  }
}
