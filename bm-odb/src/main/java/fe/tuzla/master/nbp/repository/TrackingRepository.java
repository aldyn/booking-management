package fe.tuzla.master.nbp.repository;

import com.db4o.query.Predicate;
import fe.tuzla.master.nbp.common.CommonRepository;
import fe.tuzla.master.nbp.common.DateUtils;
import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.TrackingObject;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;

@Stateless
public class TrackingRepository extends CommonRepository {

  public TrackingRepository() {
    super(TrackingObject.class);
  }

  public List<TrackingObject> findByDateAndEmployee(Date date, EmployeeObject employee) {
    return find(matchByDateAndEmployee(date, employee));
  }

  public TrackingObject findActiveByDateAndEmployee(Date date, EmployeeObject employee) {
    return findAny(matchActiveByDateAndEmployee(date, employee));
  }

  private Predicate<TrackingObject> matchActiveByDateAndEmployee(
      final Date date, final EmployeeObject employee) {
    return new Predicate<TrackingObject>() {
      public boolean match(TrackingObject tracking) {
        return tracking.getDateStr().equals(DateUtils.format(date))
            && tracking.getEndTime() == null
            && tracking.getEmployee().getId().equals(employee.getId());
      }
    };
  }

  private Predicate<TrackingObject> matchByDateAndEmployee(
      final Date date, final EmployeeObject employee) {
    return new Predicate<TrackingObject>() {
      public boolean match(TrackingObject tracking) {
        return tracking.getDateStr().equals(DateUtils.format(date))
            && tracking.getEmployee().getId().equals(employee.getId());
      }
    };
  }
}
