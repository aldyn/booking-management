package fe.tuzla.master.nbp.object;

import java.util.Date;

public interface DateIntervalHolder {

  Date getStartTime();

  void setStartTime(Date startTime);

  Date getEndTime();

  void setEndTime(Date endTime);
}
