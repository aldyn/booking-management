package fe.tuzla.master.nbp.common;

import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.object.ProjectObject;

import java.util.Date;

public class SearchParameters {
    private Date date_ = new Date();
    private ProjectObject project_;
    private JobTypeObject jobType_;
    private EmployeeObject employee_;

    public Date getDate() {
        return date_;
    }

    public void setDate(Date date) {
        date_ = date;
    }

    public ProjectObject getProject() {
        return project_;
    }

    public void setProject(ProjectObject project) {
        project_ = project;
    }

    public JobTypeObject getJobType() {
        return jobType_;
    }

    public void setJobType(JobTypeObject jobType) {
        jobType_ = jobType;
    }

    public EmployeeObject getEmployee() {
        return employee_;
    }

    public void setEmployee(EmployeeObject employee) {
        employee_ = employee;
    }
}
