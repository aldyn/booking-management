package fe.tuzla.master.nbp.object;

import java.util.Date;

public class ProjectObject implements IdHolder{
    private Integer id_;
    private String name_;
    private Date startDate_;
    private Date endDate_;

    public ProjectObject(Integer id, String name, Date startDate, Date endDate) {
        id_ = id;
        name_ = name;
        startDate_ = startDate;
        endDate_ = endDate;
    }

    public Integer getId() {
        return id_;
    }

    public void setId(Integer id) {
        id_ = id;
    }

    public String getName() {
        return name_;
    }

    public void setName(String name) {
        name_ = name;
    }

    public Date getStartDate() {
        return startDate_;
    }

    public void setStartDate(Date startDate) {
        startDate_ = startDate;
    }

    public Date getEndDate() {
        return endDate_;
    }

    public void setEndDate(Date endDate) {
        endDate_ = endDate;
    }
}
