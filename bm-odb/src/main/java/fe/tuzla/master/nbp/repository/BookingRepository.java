package fe.tuzla.master.nbp.repository;

import com.db4o.query.Predicate;
import fe.tuzla.master.nbp.common.CommonRepository;
import fe.tuzla.master.nbp.common.DateUtils;
import fe.tuzla.master.nbp.common.SearchParameters;
import fe.tuzla.master.nbp.object.BookingObject;
import fe.tuzla.master.nbp.object.EmployeeObject;

import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

@Stateless
public class BookingRepository extends CommonRepository {

    public BookingRepository() {
        super(BookingObject.class);
    }

    public List<BookingObject> findByDateAndEmployee(Date date, EmployeeObject employee) {
        return find(matchByDateAndEmployee(date, employee));
    }

    public BookingObject findByDateAndId(Date date, Integer id) {
        return findAny(matchByDateAndId(date, id));
    }

    private Predicate<BookingObject> matchByDateAndEmployee(
            final Date date, final EmployeeObject employee) {
        return new Predicate<BookingObject>() {
            public boolean match(BookingObject booking) {
                return booking.getDateStr().equals(DateUtils.format(date))
                        && booking.getEmployee().getId().equals(employee.getId());
            }
        };
    }

    private Predicate<BookingObject> matchByDateAndId(final Date date, final Integer id) {
        return new Predicate<BookingObject>() {
            public boolean match(BookingObject booking) {
                return booking.getDateStr().equals(DateUtils.format(date)) && booking.getId().equals(id);
            }
        };
    }

    public List<BookingObject> search(final SearchParameters params) {
        return find(new Predicate<BookingObject>() {
            public boolean match(BookingObject booking) {
                boolean match = true;
                if (params.getDate() != null) {
                    match = booking.getDateStr().equals(DateUtils.format(params.getDate()));
                }
                if (params.getEmployee() != null) {
                    match = match && booking.getEmployee().getId().equals(params.getEmployee().getId());
                }
                if (params.getProject() != null) {
                    match = match && booking.getProject().getId().equals(params.getProject().getId());
                }
                if (params.getJobType() != null) {
                    match = match && booking.getJobType().getId().equals(params.getJobType().getId());
                }
                return match;
            }
        });
    }
}
