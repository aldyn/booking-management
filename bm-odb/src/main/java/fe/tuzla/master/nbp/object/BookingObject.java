package fe.tuzla.master.nbp.object;

import fe.tuzla.master.nbp.common.DateUtils;
import java.util.Date;

public class BookingObject implements IdHolder, DateIntervalHolder {
  private Integer id_;
  private EmployeeObject employee_;
  private ProjectObject project_;
  private String date_;
  private String startTime_;
  private String endTime_;
  private JobTypeObject jobType_;
  private String description_;

  public BookingObject(
      Integer id,
      EmployeeObject employee,
      Date date,
      Date startTime,
      Date endTime,
      String description) {
    id_ = id;
    employee_ = employee;
    date_ = DateUtils.format(date);
    startTime_ = DateUtils.formatTime(startTime);
    endTime_ = DateUtils.formatTime(endTime);
    description_ = description;
  }

  public EmployeeObject getEmployee() {
    return employee_;
  }

  public void setEmployee(EmployeeObject employee) {
    employee_ = employee;
  }

  public ProjectObject getProject() {
    return project_;
  }

  public void setProject(ProjectObject project) {
    project_ = project;
  }

  public String getDateStr() {
    return date_;
  }

  public Date getDate() {
    return DateUtils.parse(date_);
  }

  public void setDate(Date date) {
    date_ = DateUtils.format(date);
  }

  public Date getStartTime() {
    return DateUtils.parseTime(startTime_);
  }

  public String getStartTimeStr() {
    return startTime_;
  }

  public void setStartTime(Date startTime) {
    startTime_ = DateUtils.formatTime(startTime);
  }

  public Date getEndTime() {
    return DateUtils.parseTime(endTime_);
  }

  public String getEndTimeStr() {
    return endTime_;
  }

  public void setEndTime(Date endTime) {
    endTime_ = DateUtils.formatTime(endTime);
  }

  public JobTypeObject getJobType() {
    return jobType_;
  }

  public void setJobType(JobTypeObject jobType) {
    jobType_ = jobType;
  }

  public String getDescription() {
    return description_;
  }

  public void setDescription(String description) {
    description_ = description;
  }

  public Integer getId() {
    return id_;
  }

  public void setId(Integer id) {
    id_ = id;
  }
}
