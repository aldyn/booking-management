package fe.tuzla.master.nbp.object;

import java.util.Date;

public class EmployeeObject implements IdHolder{
    private Integer id_;
    private String firstName_;
    private String lastName_;
    private Date birthDate_;
    // User login information.
    private String username_;
    private String password_;

    public EmployeeObject(Integer id, String firstName, String lastName, Date birthDate, String username, String password) {
        id_ = id;
        firstName_ = firstName;
        lastName_ = lastName;
        birthDate_ = birthDate;
        username_ = username;
        password_ = password;
    }

    public Integer getId() {
        return id_;
    }

    public void setId(Integer id) {
        id_ = id;
    }

    public String getFirstName() {
        return firstName_;
    }

    public void setFirstName(String firstName) {
        firstName_ = firstName;
    }

    public String getLastName() {
        return lastName_;
    }

    public void setLastName(String lastName) {
        lastName_ = lastName;
    }

    public Date getBirthDate() {
        return birthDate_;
    }

    public void setBirthDate(Date birthDate) {
        birthDate_ = birthDate;
    }

    public String getUsername() {
        return username_;
    }

    public void setUsername(String username) {
        username_ = username;
    }

    public String getPassword() {
        return password_;
    }

    public void setPassword(String password) {
        password_ = password;
    }
}
