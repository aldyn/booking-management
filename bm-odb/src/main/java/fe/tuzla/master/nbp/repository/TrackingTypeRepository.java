package fe.tuzla.master.nbp.repository;

import com.db4o.query.Predicate;
import fe.tuzla.master.nbp.common.CommonRepository;
import fe.tuzla.master.nbp.object.TrackingTypeObject;

import javax.ejb.Stateless;

@Stateless
public class TrackingTypeRepository extends CommonRepository {

    public TrackingTypeRepository() {
        super(TrackingTypeObject.class);
    }

    public TrackingTypeObject findById(Integer id) {
        return findAny(matchById(id));
    }

    private Predicate<TrackingTypeObject> matchById(final Integer id) {
        return new Predicate<TrackingTypeObject>() {
            public boolean match(TrackingTypeObject trackingType) {
                return trackingType.getId().equals(id);
            }
        };
    }
}
