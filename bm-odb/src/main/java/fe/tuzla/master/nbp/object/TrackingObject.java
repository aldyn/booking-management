package fe.tuzla.master.nbp.object;

import fe.tuzla.master.nbp.common.DateUtils;
import fe.tuzla.master.nbp.common.TrackingType;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TrackingObject implements IdHolder, DateIntervalHolder {
  private Integer id_;
  private String date_;
  private TrackingType type_;
  private String startTime_;
  private String endTime_;
  private EmployeeObject employee_;

  public TrackingObject(
      Integer id, Date date, TrackingType type, Date startTime, EmployeeObject employee) {
    id_ = id;
    date_ = DateUtils.format(date);
    type_ = type;
    startTime_ = DateUtils.formatTime(startTime);
    employee_ = employee;
  }

  public Integer getId() {
    return id_;
  }

  public void setId(Integer id) {
    id_ = id;
  }

  public String getDateStr() {
    return date_;
  }

  public Date getDate() {
    return DateUtils.parse(date_);
  }

  public void setDate(Date date) {
    date_ = DateUtils.format(date);
  }

  public TrackingType getType() {
    return type_;
  }

  public void setType(TrackingType type) {
    type_ = type;
  }

  public Date getStartTime() {
    return DateUtils.parseTime(startTime_);
  }

  public String getStartTimeStr() {
    return startTime_;
  }

  public void setStartTime(Date startTime) {
    startTime_ = DateUtils.formatTime(startTime);
  }

  public Date getEndTime() {
    return DateUtils.parseTime(endTime_);
  }

  public String getEndTimeStr() {
    return endTime_;
  }

  public void setEndTime(Date endTime) {
    endTime_ = DateUtils.formatTime(endTime);
  }

  public EmployeeObject getEmployee() {
    return employee_;
  }

  public void setEmployee(EmployeeObject employee) {
    employee_ = employee;
  }
}
