package fe.tuzla.master.nbp.repository;

import com.db4o.query.Predicate;
import fe.tuzla.master.nbp.common.CommonRepository;
import fe.tuzla.master.nbp.object.JobTypeObject;

import javax.ejb.Stateless;

@Stateless
public class JobRepository extends CommonRepository {

    public JobRepository() {
        super(JobTypeObject.class);
    }

    public JobTypeObject findById(Integer id) {
        return findAny(matchById(id));
    }

    private Predicate<JobTypeObject> matchById(final Integer id) {
        return new Predicate<JobTypeObject>() {
            public boolean match(JobTypeObject job) {
                return job.getId().equals(id);
            }
        };
    }
}
