package fe.tuzla.master.nbp.common;

public enum TrackingType {
    IN, PAUSE, OUT
}
