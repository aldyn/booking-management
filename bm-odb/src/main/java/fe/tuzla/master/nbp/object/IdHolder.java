package fe.tuzla.master.nbp.object;

public interface IdHolder {

    Integer getId();

    void setId(Integer id);
}
