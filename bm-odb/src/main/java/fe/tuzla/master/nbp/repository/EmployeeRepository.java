package fe.tuzla.master.nbp.repository;

import com.db4o.query.Predicate;
import fe.tuzla.master.nbp.common.CommonRepository;
import fe.tuzla.master.nbp.object.EmployeeObject;

import javax.ejb.Stateless;

@Stateless
public class EmployeeRepository extends CommonRepository {

    public EmployeeRepository() {
        super(EmployeeObject.class);
    }

    public EmployeeObject findById(Integer id) {
        return findAny(matchById(id));
    }

    public EmployeeObject findByUsernameAndPassword(String userName, String password) {
        return findAny(matchByUserNameAndPassword(userName, password));
    }

    private Predicate<EmployeeObject> matchByUserNameAndPassword(final String userName, final String password) {
        return new Predicate<EmployeeObject>() {
            public boolean match(EmployeeObject employee) {
                return employee.getUsername().equals(userName) && employee.getPassword().equals(password);
            }
        };
    }

    private Predicate<EmployeeObject> matchById(final Integer id) {
        return new Predicate<EmployeeObject>() {
            public boolean match(EmployeeObject employee) {
                return employee.getId().equals(id);
            }
        };
    }
}
