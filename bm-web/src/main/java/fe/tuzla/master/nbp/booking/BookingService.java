package fe.tuzla.master.nbp.booking;

import fe.tuzla.master.nbp.login.LoginService;
import fe.tuzla.master.nbp.object.BookingObject;
import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.repository.BookingRepository;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class BookingService {

  @Inject private BookingRepository bookingRepository_;

  public List<BookingObject> getBookings() {
    return bookingRepository_.findAll();
  }

  public List<BookingObject> getBookings(Date date) {
    return bookingRepository_.findByDateAndEmployee(date, LoginService.getActiveEmployee());
  }

  public void addNewBooking(Date selectedDate) {
    Date startTime = selectedDate;
    Date endTime = selectedDate;
    Integer id = bookingRepository_.getNextId();
    EmployeeObject employee = LoginService.getActiveEmployee();
    bookingRepository_.save(
        new BookingObject(id, employee, selectedDate, startTime, endTime, "dummy"));
  }

  public void removeBooking(Integer id, Date selectedDate) {
    BookingObject booking = bookingRepository_.findByDateAndId(selectedDate, id);
    bookingRepository_.remove(booking);
  }

  public void revert() {
    bookingRepository_.rollback();
  }

  public void save(BookingObject booking) {
    bookingRepository_.save(booking);
  }
}
