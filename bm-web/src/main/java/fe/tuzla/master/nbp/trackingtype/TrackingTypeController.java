package fe.tuzla.master.nbp.trackingtype;

import fe.tuzla.master.nbp.object.TrackingTypeObject;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class TrackingTypeController {

    private List<TrackingTypeObject> trackingTypes_;

    @EJB
    private TrackingTypeService trackingTypeService_;

    @PostConstruct
    public void initView() {
        this.trackingTypes_ = trackingTypeService_.getAllTrackingTypes();
    }

    public void addNewTrackingType() {
        trackingTypeService_.addNewTracking();
    }

    public void removeTrackingType(Integer id) {
        trackingTypeService_.removeTracking(id);
    }

    public List<TrackingTypeObject> getTrackingTypes() {
        return trackingTypes_;
    }

    public void setTrackingTypes(List<TrackingTypeObject> trackingTypes) {
        trackingTypes_ = trackingTypes;
    }
}
