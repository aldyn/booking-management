package fe.tuzla.master.nbp.booking;

import fe.tuzla.master.nbp.common.DateUtils;
import fe.tuzla.master.nbp.common.TrackingType;
import fe.tuzla.master.nbp.job.JobService;
import fe.tuzla.master.nbp.object.BookingObject;
import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.object.ProjectObject;
import fe.tuzla.master.nbp.object.TrackingObject;
import fe.tuzla.master.nbp.project.ProjectService;
import fe.tuzla.master.nbp.tracking.TrackingService;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Named
@SessionScoped
public class BookingController implements Serializable {

    private Date editingStartTime_;

    private Date selectedDate_;

    private List<BookingObject> bookings_;

    @EJB
    private BookingService bookingService_;

    @EJB
    private ProjectService projectService_;

    @EJB
    private JobService jobService_;

    @EJB
    private TrackingService trackingService_;

    @PostConstruct
    public void initView() {
        selectedDate_ = new Date();
        bookings_ = bookingService_.getBookings(selectedDate_);
    }

    public void onRowEdit(RowEditEvent event) {
        BookingObject booking = (BookingObject) event.getObject();
        bookingService_.save(booking);
    }

    public void onRowCancel(RowEditEvent event) {
        BookingObject booking = (BookingObject) event.getObject();
        bookingService_.revert();
    }

    public void onDateSelect(SelectEvent event) {
        bookings_ = bookingService_.getBookings(selectedDate_);
    }

    public void onStartTimeChange(SelectEvent event) {
        editingStartTime_ = (Date) event.getObject();
    }

    public String getTimeDiff(BookingObject booking) {
        return DateUtils.getTimeDiffAsStr(booking.getStartTime(), booking.getEndTime());
    }

    public void addNewBooking() {
        bookingService_.addNewBooking(selectedDate_);
        this.onDateSelect(null);
    }

    public void removeBooking(Integer id) {
        bookingService_.removeBooking(id, selectedDate_);
        this.onDateSelect(null);
    }

    public int getMinHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(editingStartTime_ != null ? editingStartTime_ : date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public int getMinMinutes(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(editingStartTime_ != null ? editingStartTime_ : date);
        return calendar.get(Calendar.MINUTE);
    }

    public int getMinSeconds(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(editingStartTime_ != null ? editingStartTime_ : date);
        return calendar.get(Calendar.SECOND);
    }

    public String bookedTime() {
        return DateUtils.sumIntervals(bookings_);
    }

    public String spentTime() {
        List<TrackingObject> trackings = trackingService_.getTrackings(selectedDate_);
        List<TrackingObject> temp = new ArrayList<TrackingObject>();
        for (TrackingObject tracking : trackings) {
            if (tracking.getType() != TrackingType.PAUSE) {
                temp.add(tracking);
            }
        }
        return DateUtils.sumIntervals(temp);
    }

    public boolean bookingFine() {
        String spentTime = this.spentTime();
        String bookedTime = this.bookedTime();
        if (spentTime != null && bookedTime != null) {
            return spentTime.equals(bookedTime);
        }
        return false;
    }

    public List<ProjectObject> getProjects() {
        return projectService_.getAllProjects();
    }

    public List<JobTypeObject> getJobTypes() {
        return jobService_.getAllJobs();
    }

    public Date getDateFromTime(Time time) {
        return new Date(time.getTime());
    }

    public Date getSelectedDate() {
        return selectedDate_;
    }

    public void setSelectedDate(Date selectedDate) {
        selectedDate_ = selectedDate;
    }

    public List<BookingObject> getBookings() {
        return bookings_;
    }

    public void setBookings(List<BookingObject> bookings) {
        bookings_ = bookings;
    }
}
