package fe.tuzla.master.nbp.employee;

import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.JobTypeObject;
import org.primefaces.event.RowEditEvent;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class EmployeeController {

    private List<EmployeeObject> employees_;

    @EJB
    private EmployeeService employeeService_;

    @PostConstruct
    public void initView() {
        this.employees_ = employeeService_.getAllUsers();
    }

    public void onRowEdit(RowEditEvent event) {
        EmployeeObject employee = (EmployeeObject) event.getObject();
        employeeService_.save(employee);
    }

    public void onRowCancel(RowEditEvent event) {
        employeeService_.revert();
    }

    public String getPasswordMask(String password) {
        return new String(new char[password.length()]).replace("\0", "*");
    }

    public void addNewEmployee() {
        employeeService_.addNewEmployee();
    }

    public void removeEmployee(Integer id) {
        employeeService_.removeEmployee(id);
    }

    public List<EmployeeObject> getEmployees() {
        return employees_;
    }

    public void setEmployees(List<EmployeeObject> employees) {
        employees_ = employees;
    }
}
