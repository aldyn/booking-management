package fe.tuzla.master.nbp.job;

import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.object.ProjectObject;
import fe.tuzla.master.nbp.repository.JobRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class JobService {

    @Inject
    private JobRepository jobRepository_;

    public List<JobTypeObject> getAllJobs() {
        return jobRepository_.findAll();
    }

    public void revert() {
        jobRepository_.rollback();
    }

    public void save(JobTypeObject jobType) {
        jobRepository_.save(jobType);
    }

    public void addNewJob() {
        jobRepository_.save(new JobTypeObject(jobRepository_.getNextId(), "dummy", "dummy"));
    }

    public void removeJob(Integer id) {
        jobRepository_.remove(jobRepository_.findById(id));
    }
}
