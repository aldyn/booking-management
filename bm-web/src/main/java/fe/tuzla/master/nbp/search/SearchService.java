package fe.tuzla.master.nbp.search;

import fe.tuzla.master.nbp.common.SearchParameters;
import fe.tuzla.master.nbp.object.BookingObject;
import fe.tuzla.master.nbp.repository.BookingRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class SearchService {

    @Inject
    private BookingRepository bookingRepository_;

    public List<BookingObject> search(SearchParameters params){
        return bookingRepository_.search(params);
    }
}
