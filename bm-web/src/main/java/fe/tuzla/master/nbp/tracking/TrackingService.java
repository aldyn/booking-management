package fe.tuzla.master.nbp.tracking;

import fe.tuzla.master.nbp.common.TrackingType;
import fe.tuzla.master.nbp.login.LoginService;
import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.TrackingObject;
import fe.tuzla.master.nbp.repository.TrackingRepository;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class TrackingService {

  @Inject private TrackingRepository trackingRepository_;

  public List<TrackingObject> getTrackings() {
    Date today = new Date();
    return trackingRepository_.findByDateAndEmployee(today, LoginService.getActiveEmployee());
  }

  public List<TrackingObject> getTrackings(Date date) {
    return trackingRepository_.findByDateAndEmployee(date, LoginService.getActiveEmployee());
  }

  public String save(TrackingType trackingType) {
    Date currentTime = new Date();
    TrackingObject trackingObject = getActiveTracking();

    if (trackingObject != null) {
      boolean wasPause = trackingObject.getType() == TrackingType.PAUSE;
      boolean isInType = trackingType == TrackingType.IN;
      if (isInType && !wasPause) {
        return "Tracking of type `IN` is not allowed for non-finished previous tracking!";
      }
      trackingObject.setEndTime(currentTime);
      if (trackingType == TrackingType.PAUSE && !wasPause) {
        trackingObject.setType(TrackingType.OUT);
        createNewTracking(TrackingType.PAUSE);
      } else if (wasPause) {
        trackingObject.setType(TrackingType.PAUSE);
        if (isInType) {
          createNewTracking(trackingType);
        }
      } else {
        trackingObject.setType(trackingType);
      }
      trackingRepository_.save(trackingObject);
    } else {
      if (trackingType != TrackingType.IN) {
        if (trackingType == TrackingType.PAUSE) {
          return "Tracking of type `PAUSE` is not allowed without previous tracking of type`IN`!";
        } else {
          return "Tracking of type `OUT` is not allowed without previous tracking of type`IN`!";
        }
      }
      createNewTracking(trackingType);
    }

    return null;
  }

  private void createNewTracking(TrackingType type) {
    Date today = new Date();
    Date currentTime = new Date();
    EmployeeObject employee = LoginService.getActiveEmployee();

    Integer id = trackingRepository_.getNextId();
    trackingRepository_.save(new TrackingObject(id, today, type, currentTime, employee));
  }

  private TrackingObject getActiveTracking() {
    java.util.Date today = new java.util.Date();
    Date dateToday = new Date(today.getTime());
    return trackingRepository_.findActiveByDateAndEmployee(
        dateToday, LoginService.getActiveEmployee());
  }
}
