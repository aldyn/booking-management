package fe.tuzla.master.nbp.job;

import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.object.ProjectObject;
import org.primefaces.event.RowEditEvent;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class JobController {

    private List<JobTypeObject> jobs_;

    @EJB
    private JobService jobService_;

    @PostConstruct
    public void initView() {
        this.jobs_ = jobService_.getAllJobs();
    }

    public void onRowEdit(RowEditEvent event) {
        JobTypeObject jobType = (JobTypeObject) event.getObject();
        jobService_.save(jobType);
    }

    public void onRowCancel(RowEditEvent event) {
        jobService_.revert();
    }

    public void addNewJob() {
        jobService_.addNewJob();
    }

    public void removeJob(Integer id) {
        jobService_.removeJob(id);
    }

    public List<JobTypeObject> getJobs() {
        return jobs_;
    }

    public void setJobs(List<JobTypeObject> jobs) {
        jobs_ = jobs;
    }
}
