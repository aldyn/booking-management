package fe.tuzla.master.nbp.login;

import fe.tuzla.master.nbp.employee.EmployeeService;
import fe.tuzla.master.nbp.object.EmployeeObject;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LoginService implements Serializable {
  private static final String WELCOME_PATH = "/modules/mod-welcome/welcome.xhtml";
  private static final String LOGOUT_PATH = "/modules/mod-logout/logout.xhtml";
  @EJB private EmployeeService employeeService;

  private String username;
  private String password;
  private boolean userLoggedIn;

  public static EmployeeObject getActiveEmployee() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    return (EmployeeObject) externalContext.getSessionMap().get("employee");
  }

  public void login() throws IOException {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();

    EmployeeObject employee = employeeService.findUser(username, password);
    if (employee != null) {
      externalContext.getSessionMap().put("employee", employee);
      externalContext.redirect(externalContext.getRequestContextPath() + WELCOME_PATH);
      userLoggedIn = true;
    } else {
      context.addMessage(
          null,
          new FacesMessage(
              FacesMessage.SEVERITY_WARN, "Login failed!", "Incorrect username or password"));
    }
  }

  public void logout() throws IOException {
    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
    externalContext.invalidateSession();
    userLoggedIn = false;
    externalContext.redirect(externalContext.getRequestContextPath() + LOGOUT_PATH);
  }

  public boolean isUserLoggedIn() {
    return userLoggedIn;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
