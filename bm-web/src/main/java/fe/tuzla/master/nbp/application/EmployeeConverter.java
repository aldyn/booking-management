package fe.tuzla.master.nbp.application;

import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.ProjectObject;
import fe.tuzla.master.nbp.repository.EmployeeRepository;
import fe.tuzla.master.nbp.repository.ProjectRepository;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class EmployeeConverter implements Converter {
    @Inject
    private EmployeeRepository employeeRepository_;

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null) {
            return null;
        }
        Integer id = Integer.valueOf(s);
        return employeeRepository_.findById(id);
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (!(o instanceof EmployeeObject)) {
            return null;
        }
        EmployeeObject employee = (EmployeeObject) o;
        return String.valueOf(employee.getId());
    }
}
