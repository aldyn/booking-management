package fe.tuzla.master.nbp.project;

import fe.tuzla.master.nbp.object.BookingObject;
import fe.tuzla.master.nbp.object.ProjectObject;
import fe.tuzla.master.nbp.repository.ProjectRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
public class ProjectService {

    @Inject
    private ProjectRepository projectRepository_;

    public List<ProjectObject> getAllProjects() {
        return projectRepository_.findAll();
    }

    public void revert() {
        projectRepository_.rollback();
    }

    public void save(ProjectObject project) {
        projectRepository_.save(project);
    }

    public void addNewProject() {
        projectRepository_.save(new ProjectObject(projectRepository_.getNextId(), "dummy", new Date(), new Date()));
    }

    public void removeProject(Integer id) {
        projectRepository_.remove(projectRepository_.findById(id));
    }
}
