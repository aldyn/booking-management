package fe.tuzla.master.nbp.application;

import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.repository.JobRepository;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class JobTypeConverter implements Converter {
    @Inject
    private JobRepository jobRepository_;

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null) {
            return null;
        }
        Integer id = Integer.valueOf(s);
        return jobRepository_.findById(id);
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (!(o instanceof JobTypeObject)) {
            return null;
        }
        JobTypeObject jobType = (JobTypeObject) o;
        return String.valueOf(jobType.getId());
    }
}
