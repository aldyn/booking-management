package fe.tuzla.master.nbp.application;

import fe.tuzla.master.nbp.object.ProjectObject;
import fe.tuzla.master.nbp.repository.ProjectRepository;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ProjectConverter implements Converter {
    @Inject
    private ProjectRepository projectRepository_;

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null) {
            return null;
        }
        Integer id = Integer.valueOf(s);
        return projectRepository_.findById(id);
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (!(o instanceof ProjectObject)) {
            return null;
        }
        ProjectObject project = (ProjectObject) o;
        return String.valueOf(project.getId());
    }
}
