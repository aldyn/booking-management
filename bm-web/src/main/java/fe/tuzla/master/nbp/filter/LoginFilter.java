package fe.tuzla.master.nbp.filter;

import fe.tuzla.master.nbp.login.LoginService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "LoginFilter", urlPatterns = {"/modules/*"})
public class LoginFilter implements Filter {

    private static final String LOGIN_PATH = "/modules/mod-login/login.xhtml";

    /**
     * Checks if user is logged in. If not it redirects to the login.xhtml page.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        LoginService loginService = (LoginService) ((HttpServletRequest) request).getSession().getAttribute("loginService");
        // For the first application request there is no loginBean in the session so user needs to log in
        // For other requests loginBean is present but we need to check if user has logged in successfully
        if ((loginService == null || !loginService.isUserLoggedIn()) && !((HttpServletRequest) request).getRequestURI().contains(LOGIN_PATH)) {
            String contextPath = ((HttpServletRequest) request).getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath + LOGIN_PATH);
        }
        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {
        // Nothing to do here!
    }

    public void destroy() {
        // Nothing to do here!
    }

}