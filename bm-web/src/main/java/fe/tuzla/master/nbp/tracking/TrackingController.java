package fe.tuzla.master.nbp.tracking;

import fe.tuzla.master.nbp.common.DateUtils;
import fe.tuzla.master.nbp.common.TrackingType;
import fe.tuzla.master.nbp.object.TrackingObject;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class TrackingController implements Serializable {

    private static TrackingType focusedTracking_ = TrackingType.IN;
    private List<TrackingObject> trackings_;

    @EJB
    private TrackingService trackingService_;

    @PostConstruct
    public void initView() {
        trackings_ = trackingService_.getTrackings();
    }

    public void save(TrackingType trackingType) {
        FacesContext context = FacesContext.getCurrentInstance();
        String message = trackingService_.save(trackingType);
        if (message == null) {
            context.addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "Tracking OK"));
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed", message));
        }
    }

    public void changeFocus(TrackingType type) {
        focusedTracking_ = type;
    }

    public String getTimeDiff(TrackingObject tracking) {
        return DateUtils.getTimeDiffAsStr(tracking.getStartTime(), tracking.getEndTime());
    }

    public String getProductiveHours() {
        List<TrackingObject> temp = new ArrayList<TrackingObject>();
        for (TrackingObject tracking : trackings_) {
            if (tracking.getType() != TrackingType.PAUSE) {
                temp.add(tracking);
            }
        }
        return DateUtils.sumIntervals(temp);
    }

    public TrackingType getFocusedTracking() {
        return focusedTracking_;
    }

    public List<TrackingObject> getTrackings() {
        return trackings_;
    }

    public void setTrackings(List<TrackingObject> trackings) {
        trackings_ = trackings;
    }
}
