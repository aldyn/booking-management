package fe.tuzla.master.nbp.employee;

import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.repository.EmployeeRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
public class EmployeeService {

    @Inject
    private EmployeeRepository employeeRepository_;

    public EmployeeObject findUser(String userName, String password) {
        return employeeRepository_.findByUsernameAndPassword(userName, password);
    }

    public void revert() {
        employeeRepository_.rollback();
    }

    public void save(EmployeeObject employee) {
        employeeRepository_.save(employee);
    }

    public List<EmployeeObject> getAllUsers() {
        return employeeRepository_.findAll();
    }

    public void addNewEmployee() {
        employeeRepository_.save(new EmployeeObject(employeeRepository_.getNextId(), "dummy", "dummy", new Date(), "dummy", "dummy"));
    }

    public void removeEmployee(Integer id) {
        employeeRepository_.remove(employeeRepository_.findById(id));
    }
}
