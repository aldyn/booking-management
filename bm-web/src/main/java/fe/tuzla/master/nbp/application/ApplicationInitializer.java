package fe.tuzla.master.nbp.application;

import fe.tuzla.master.nbp.api.Db4oApi;
import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.object.ProjectObject;
import fe.tuzla.master.nbp.object.TrackingTypeObject;
import fe.tuzla.master.nbp.repository.EmployeeRepository;
import fe.tuzla.master.nbp.repository.JobRepository;
import fe.tuzla.master.nbp.repository.ProjectRepository;
import fe.tuzla.master.nbp.repository.TrackingTypeRepository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import java.util.Date;

@ManagedBean(eager = true)
@ApplicationScoped
public class ApplicationInitializer {

    @Inject
    private EmployeeRepository employeeRepository_;

    @Inject
    private JobRepository jobRepository_;

    @Inject
    private TrackingTypeRepository trackingTypeRepository_;

    @Inject
    private ProjectRepository projectRepository_;

    @PostConstruct
    public void initializeData() {
        Db4oApi.getInstance().openConnection();
//        initData();
    }

    private void initData(){
      employeeRepository_.save(new EmployeeObject(0, "Aldin", "Colic", new Date(), "aldin", "1"));
      employeeRepository_.save(new EmployeeObject(1, "Alen", "Colic", new Date(), "alen", "1"));

      jobRepository_.save(new JobTypeObject(0, "Programming", "Used when the employee spent time in programming something."));
      jobRepository_.save(new JobTypeObject(1, "Meeting", "Used when the employee spent time in meeting."));
      jobRepository_.save(new JobTypeObject(2, "Architectural design", "Used when the employee spent time in architectural design."));

      trackingTypeRepository_.save(new TrackingTypeObject(0, "Input", "Employee came in the office."));
      trackingTypeRepository_.save(new TrackingTypeObject(1, "Pause", "Employee is on the lunch break."));
      trackingTypeRepository_.save(new TrackingTypeObject(2, "Output", "Employee left the office."));

      projectRepository_.save(new ProjectObject(0, "Argus", new Date(), new Date()));
      projectRepository_.save(new ProjectObject(1, "Pro|Duct", new Date(), new Date()));
    }

    @PreDestroy
    public void closeConnection() {
        Db4oApi.getInstance().closeConnection();
    }
}
