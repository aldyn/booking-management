package fe.tuzla.master.nbp.trackingtype;

import fe.tuzla.master.nbp.object.TrackingTypeObject;
import fe.tuzla.master.nbp.repository.TrackingTypeRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class TrackingTypeService {

    @Inject
    private TrackingTypeRepository trackingTypeRepository_;

    public List<TrackingTypeObject> getAllTrackingTypes() {
        return trackingTypeRepository_.findAll();
    }

    public void addNewTracking() {
        trackingTypeRepository_.save(new TrackingTypeObject(trackingTypeRepository_.getNextId(), "dummy", "dummy"));
    }

    public void removeTracking(Integer id) {
        trackingTypeRepository_.remove(trackingTypeRepository_.findById(id));
    }
}
