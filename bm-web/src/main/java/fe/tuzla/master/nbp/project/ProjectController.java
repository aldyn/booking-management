package fe.tuzla.master.nbp.project;

import fe.tuzla.master.nbp.object.ProjectObject;
import org.primefaces.event.RowEditEvent;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class ProjectController {

    private List<ProjectObject> projects_;

    @EJB
    private ProjectService projectService_;

    @PostConstruct
    public void initView() {
        this.projects_ = projectService_.getAllProjects();
    }

    public void onRowEdit(RowEditEvent event) {
        ProjectObject project = (ProjectObject) event.getObject();
        projectService_.save(project);
    }

    public void onRowCancel(RowEditEvent event) {
        projectService_.revert();
    }

    public void addNewProject() {
        projectService_.addNewProject();
    }

    public void removeProject(Integer id) {
        projectService_.removeProject(id);
    }

    public List<ProjectObject> getProjects() {
        return projects_;
    }

    public void setProjects(List<ProjectObject> projects) {
        projects_ = projects;
    }
}
