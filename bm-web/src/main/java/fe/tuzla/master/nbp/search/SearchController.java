package fe.tuzla.master.nbp.search;

import fe.tuzla.master.nbp.booking.BookingService;
import fe.tuzla.master.nbp.common.SearchParameters;
import fe.tuzla.master.nbp.employee.EmployeeService;
import fe.tuzla.master.nbp.job.JobService;
import fe.tuzla.master.nbp.object.BookingObject;
import fe.tuzla.master.nbp.object.EmployeeObject;
import fe.tuzla.master.nbp.object.JobTypeObject;
import fe.tuzla.master.nbp.object.ProjectObject;
import fe.tuzla.master.nbp.project.ProjectService;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class SearchController implements Serializable {

  private SearchParameters searchParameters_;

  private List<BookingObject> bookings_;

  @EJB private BookingService bookingService_;

  @EJB private EmployeeService employeeService_;

  @EJB private ProjectService projectService_;

  @EJB private JobService jobService_;

  @EJB private SearchService searchService_;

  @PostConstruct
  public void initView() {
//    bookings_ = bookingService_.getBookings();
      searchParameters_ = new SearchParameters();
  }

  public void search(){
    bookings_ = searchService_.search(searchParameters_);
  }

  public List<EmployeeObject> getEmployees() {
    return employeeService_.getAllUsers();
  }

  public List<ProjectObject> getProjects() {
    return projectService_.getAllProjects();
  }

  public List<JobTypeObject> getJobTypes() {
    return jobService_.getAllJobs();
  }

  public Date getDateFromTime(Time time) {
    return new Date(time.getTime());
  }

  public List<BookingObject> getBookings() {
    return bookings_;
  }

  public void setBookings(List<BookingObject> bookings) {
    bookings_ = bookings;
  }

    public SearchParameters getSearchParameters() {
        return searchParameters_;
    }

  public void setSearchParameters(SearchParameters searchParameters) {
    searchParameters_ = searchParameters;
  }
}
